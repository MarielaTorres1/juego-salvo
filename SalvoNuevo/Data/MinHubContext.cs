﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using SalvoNuevo.Models;

namespace SalvoNuevo.Models
{
    public class MinHubContext : DbContext
    {
       /* public MinHubContext()
        {

        }*/

        public MinHubContext (DbContextOptions<MinHubContext> options)
            : base(options)
        {

        }
        /*
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer(@"Server=ECOTECH-PC\SQLEXPRESS;Database=MinHub2;Trusted_Connection=True;MultipleActiveResultSets=true");
            }
        }*/
        public DbSet<Player> Player { get; set; }
        public DbSet<Game> Game { get; set; }
        public DbSet<GamePlayer> GamePlayer { get; set; }
        public DbSet<Ship> Ship { get; set; }
        public DbSet<ShipLocations> ShipLocations { get; set; }
        public DbSet<Salvo> Salvos { get; set; }
        public DbSet<SalvoLocations> SalvoLocations { get; set; }
        public DbSet<Score> Score { get; set; }
    }
}
