﻿using SalvoNuevo.Models;
using SalvoNuevo.ModelsDTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SalvoNuevo.Repositories
{
    public interface IGamePlayerRepository
    {
        IEnumerable <GamePlayer> GetGamePlayerList();
        void Save(GamePlayer gamePlayer);
        GamePlayer GetGamePlayerDTO(long idGamePlayer);
        GamePlayer FindById(long id);
    }
}
