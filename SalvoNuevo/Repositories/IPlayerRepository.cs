﻿using SalvoNuevo.Models;
using SalvoNuevo.ModelsDTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SalvoNuevo.Repositories
{
    public interface IPlayerRepository
    {
        IEnumerable <Player> GetPlayer();
        Player GetUniquePlayer(long Id);
        Player FindByEmail(string Email);
        void Save(Player player);
    }
}
