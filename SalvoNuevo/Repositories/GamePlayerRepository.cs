﻿using SalvoNuevo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using SalvoNuevo.ModelsDTO;

namespace SalvoNuevo.Repositories
{
    public class GamePlayerRepository : RepositoryBase<GamePlayer>, IGamePlayerRepository
    {
        public GamePlayerRepository(MinHubContext repositoryContext)
             : base(repositoryContext)
        {
        }
        public IEnumerable <GamePlayer> GetGamePlayerList()
        {
           
                return FindAll(gp => gp.Include(x => x.Game).Include(x => x.Player))
                        .ToList();
            
        }/*
        public IEnumerable <GamePlayer> GetGamePlayerList(long? idGame)
        {
          

                return context.GamePlayer.Where(r => r.Gameid == idGame).ToList();
            
        }*/

        public GamePlayer GetGamePlayerDTO(long idGamePlayer)
        {

            return FindAll(source => source.Include(gamePlayer => gamePlayer.Ships)
                                                 .ThenInclude(ship => ship.Locations)
                                             .Include(gamePlayer => gamePlayer.Salvos)
                                                 .ThenInclude(salvo => salvo.Locations)
                                             .Include(gamePlayer => gamePlayer.Game)
                                                 .ThenInclude(game => game.GamePlayer)
                                                     .ThenInclude(gp => gp.Player)
                                             .Include(gamePlayer => gamePlayer.Game)
                                                 .ThenInclude(game => game.GamePlayer)
                                                     .ThenInclude(gp => gp.Salvos)
                                                     .ThenInclude(salvo => salvo.Locations)
                                             .Include(gamePlayer => gamePlayer.Game)
                                                 .ThenInclude(game => game.GamePlayer)
                                                     .ThenInclude(gp => gp.Ships)
                                                     .ThenInclude(ship => ship.Locations)
                                             )
                 .Where(gamePlayer => gamePlayer.Id == idGamePlayer)
                 .OrderBy(game => game.CreationDate)
                 .FirstOrDefault();
        }
        public GamePlayer FindById(long id)
        {
            return FindAll(source => source.Include(gamePlayer => gamePlayer.Ships)
                                                    .ThenInclude(ship => ship.Locations)
                                            .Include(gamePlayer => gamePlayer.Salvos)
                                                    .ThenInclude(salvo => salvo.Locations)
                                             .Include(gamePlayer => gamePlayer.Game)
                                                    .ThenInclude(game => game.GamePlayer)
                                                        .ThenInclude(gp => gp.Player))
                                                    .Where(gamePlayer => gamePlayer.Id == id)
                                                    .FirstOrDefault();
        }
        public void Save(GamePlayer gamePlayer)
        {
            if (gamePlayer.Id == 0)
                Create(gamePlayer);
            else
                Update(gamePlayer);
            SaveChanges();
        }
        
    }
}
