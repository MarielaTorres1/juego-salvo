﻿using SalvoNuevo.Models;
using SalvoNuevo.ModelsDTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SalvoNuevo.Repositories
{
    public interface IGameRepository
    {
        IEnumerable<Game> GetAllGames(); 
        Game GetUniqueGame(long Id);
        IEnumerable<Game> GetScore();
        void Save(Game game);
        Game FindById(long id);
    }
}
