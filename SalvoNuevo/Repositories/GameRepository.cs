﻿using Microsoft.EntityFrameworkCore;
using SalvoNuevo.Models;
using SalvoNuevo.ModelsDTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SalvoNuevo.Repositories
{
    public class GameRepository : RepositoryBase<Game>, IGameRepository
    {
        public GameRepository(MinHubContext repositoryContext)
                : base(repositoryContext)
        {
        }

        public IEnumerable<Game> GetAllGames()
        {
            var listGameDB= FindAll(source => source.Include(game => game.GamePlayer).ThenInclude(gameplayer => gameplayer.Player)
                    .Include(game => game.GamePlayer).ThenInclude(gp => gp.Ships).ThenInclude(ship => ship.Locations)
            )
                .ToList();
 
            return listGameDB;
        }
    
        public Game GetUniqueGame(long id)
        {
            
                var gamedb = FindAll(gameTabla => gameTabla
                    .Include(game => game.GamePlayer)
                        .ThenInclude(gameplayer => gameplayer.Player)
                    .Include(game => game.GamePlayer)
                        .ThenInclude(gp => gp.Ships)
                        .ThenInclude(ship => ship.Locations))
                    .ToList().FirstOrDefault(p => p.id == id);
                return gamedb;
            
        }
        public IEnumerable<Game> GetScore()
        {
            return FindAll(source => source.Include(g => g.GamePlayer)
                                                  .ThenInclude(gp => gp.Player)
                                            .Include(x => x.GamePlayer)
                                                  .ThenInclude(y => y.Ships)
                                                      .ThenInclude(b => b.Locations)
                                             .Include(g => g.GamePlayer)
                                                  .ThenInclude(gp => gp.Player)
                                             .ThenInclude(p => p.Score))
                                                .ToList();
        }
        public Game FindById(long id)
        {
            return FindAll(source => source.Include(g => g.GamePlayer)
                                              .ThenInclude(gp => gp.Player))
                  .ToList().FirstOrDefault(p => p.id == id);
        }
        public void Save(Game game)
        {
            Create(game);
            SaveChanges();
        }

    }
}

  
