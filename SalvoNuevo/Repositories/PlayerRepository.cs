﻿using SalvoNuevo;
using SalvoNuevo.Models;
using SalvoNuevo.ModelsDTO;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;                                                                                 

namespace SalvoNuevo.Repositories
{
    public class PlayerRepository : RepositoryBase<Player>, IPlayerRepository
    {
        public PlayerRepository(MinHubContext repositoryContext)
        : base(repositoryContext)
        {
        }

        public IEnumerable <Player> GetPlayer()
        {
            return FindAll().ToList();
        }
        public Player GetUniquePlayer(long Id)
        {

            return FindAll().FirstOrDefault(p => p.Id == Id);
                
        }
        public Player FindByEmail(string Email)
        {
            return FindAll().FirstOrDefault(p => p.Email == Email);
        }
        public void Save(Player player)
        {
            Create(player);
            SaveChanges();
        }
    }
}
