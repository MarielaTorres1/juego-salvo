﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SalvoNuevo.Models
{
    public class Player
    {
        public long Id { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public ICollection<GamePlayer> GamePlayer { get; set; }
        public ICollection<Score> Score { get; set; }

        public Score GetScore(Game game)
        {
            return Score.FirstOrDefault(score => score.GameId == game.id);
        }
       
    }
}
