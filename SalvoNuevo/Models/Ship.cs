﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SalvoNuevo.Models
{
    public class Ship
    {
        public long Id { get; set; }
        public string Type { get; set; }
        public int Lenght { get; set; }
        public GamePlayer GamePlayer { get; set; }
        public long GamePlayerID { get; set; }
        public ICollection<ShipLocations> Locations { get; set; }
            
    }
}
