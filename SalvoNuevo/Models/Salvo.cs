﻿using SalvoNuevo.ModelsDTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SalvoNuevo.Models
{
    public class Salvo
    {
        public long Id { get; set; }
        public int Turn { get; set; }
        public GamePlayer GamePlayer { get; set; }
        public ICollection<SalvoLocations> Locations { get; set; }

        public List<string> ListaLocations() 
        {
            List<string> listaLoc = new List<string>();

            foreach (var loc in Locations)
            {
                listaLoc.Add(loc.Location);
            }
            return listaLoc;
        }
    }
}
