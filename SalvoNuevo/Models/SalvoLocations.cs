﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SalvoNuevo.Models
{
    public class SalvoLocations
    {
        public long Id { get; set; }
        public Salvo Salvo { get; set; }
        public string Location { get; set; }
    }
}
