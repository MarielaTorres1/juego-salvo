﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SalvoNuevo.Models
{
    public class Game
    {
        //get y set me permite manipular las variables
        public long id { get; set; }
        public DateTime? CreationDate { get; set; }
        public List<GamePlayer> GamePlayer { get; set; }
        public ICollection<Score> Score { get; set; }

    }
}
