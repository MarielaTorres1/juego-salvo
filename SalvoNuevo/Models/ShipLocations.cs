﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SalvoNuevo.Models
{
    public class ShipLocations
    {
        public long Id { get; set; }
        public string Location { get; set; }
        public Ship Ship { get; set; }
    }
}
