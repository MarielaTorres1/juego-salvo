﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SalvoNuevo.Models
{
    public class GamePlayer
    {
        public long Id { get; set; }
        public long Playerid { get; set; }
        public long Gameid { get; set; }
        public DateTime? CreationDate { get; set; }
        public Game Game { get; set; }
        public Player Player { get; set; }
        public ICollection<Ship> Ships { get; set; }
        public ICollection<Salvo> Salvos { get; set; }

        public GamePlayer GetOpponent()
        {
            foreach(var gpOpponent in Game.GamePlayer)
            {
                if(gpOpponent.Id != Id)
                {
                    return gpOpponent;
                }
            }
            return null;
        }


    }
}
