﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SalvoNuevo.Models;
using SalvoNuevo.ModelsDTO;
using SalvoNuevo.Repositories;

namespace SalvoNuevo.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PlayersController : ControllerBase

    {
        private IPlayerRepository _repository;

        public PlayersController(IPlayerRepository repository)
        {
            _repository = repository;
        }

        // GET: api/Players
        [HttpGet]
        public IActionResult Get()
        {
            var players = _repository.GetPlayer();

            List<PlayerDTO> playersDTO = new List<PlayerDTO>();
            foreach (var player in players)
            {
                var p = new PlayerDTO(player);
                playersDTO.Add(p);
            }
            return Ok(players);
        }

        // GET: api/Players/5
        [HttpGet("{id}", Name = "GetPlayer")]
        public IActionResult Get(long id)
        {
            var uniquePlayer = _repository.GetUniquePlayer(id);
            return Ok(uniquePlayer);
        }

        [HttpPost]
        public IActionResult Post([FromBody] PlayerDTO player)
        {
            try
            {
                if (string.IsNullOrEmpty(player.Email) || string.IsNullOrEmpty(player.Password))
                {
                    return StatusCode(403, "Datos ingresados invalidos.");
                }

                else if (_repository.FindByEmail(player.Email) != null)
                {
                    return StatusCode(403, "Email en uso.");
                }
                else
                {
                    var newPlayer = new Player();
                    newPlayer.Email = player.Email;
                    newPlayer.Password = player.Password;

                    _repository.Save(newPlayer);
                    
                    return StatusCode(201, newPlayer);

                }
            }
            catch (Exception ex)
            {
                return StatusCode(500, "Internal server error");
            }


        }
    }
}