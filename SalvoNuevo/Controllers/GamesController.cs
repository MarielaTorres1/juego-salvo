﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SalvoNuevo.Models;
using SalvoNuevo.ModelsDTO;
using SalvoNuevo.Repositories;

namespace SalvoNuevo.Controllers
{
    [Route("api/games")]
    [ApiController]
    [Authorize]
    public class GamesController : ControllerBase
    {
        private IGameRepository _repository;
        private IPlayerRepository _repositoryP;
        private IGamePlayerRepository _repositoryGP;

        string Email;

        public GamesController(IGameRepository repository, IPlayerRepository repositoryP, IGamePlayerRepository repositoryGP)
        {
            _repository = repository;
            _repositoryP = repositoryP;
            _repositoryGP = repositoryGP;

        }

        [HttpGet]
        [AllowAnonymous]
        public IActionResult Get()
        {
            var games = _repository.GetScore();
            List<GameDTO> g = new List<GameDTO>();
            foreach (var game in games)
            {
                var gameDTO = new GameDTO(game);
                g.Add(gameDTO);
            }
            Email = User.FindFirst("Player") != null ? User.FindFirst("Player").Value : "Guest";

            return Ok(new GuestPlayerDTO(g, Email));
        }

        [HttpGet("{id}")]
        public IActionResult Get(long Id)
        {

            return Ok(new GameDTO(_repository.GetUniqueGame(Id)));
        }
        [HttpPost]
        public IActionResult CreateGamePlayer()
        {
            string EmailSession = User.FindFirst("Player").Value;
            long IdSession = _repositoryP.FindByEmail(EmailSession).Id;

            Game game = new Game();
            game.CreationDate = DateTime.Now;

            GamePlayer nuevoGamePlayer = new GamePlayer();
            nuevoGamePlayer.CreationDate = DateTime.Now;
            nuevoGamePlayer.Game = game;
            nuevoGamePlayer.Playerid = IdSession;
            _repositoryGP.Save(nuevoGamePlayer);

            return StatusCode(201, nuevoGamePlayer.Id);
        }
        ///api/games/4/players
        [HttpPost("{id}/players")]
        public IActionResult Post(long id)
        {
           
            string EmailSession = User.FindFirst("Player").Value;
            Game Game = _repository.FindById(id);
            long IdSession = _repositoryP.FindByEmail(EmailSession).Id;
            try
            {
                if (Game == null)
                {
                    return StatusCode(403, "No existe el juego.");
                }
                else if (Game.GamePlayer.Count == 2)
                {
                    return StatusCode(403, "Juego lleno.");
                }
                else if (Game.GamePlayer.FirstOrDefault().Playerid == _repositoryP.FindByEmail(EmailSession).Id)
                {
                    return StatusCode(403, "Ya se encuentra el jugador en el juego.");
                }
                else
                {

                    GamePlayer nuevoGamePlayer = new GamePlayer();
                    nuevoGamePlayer.CreationDate = DateTime.Now;
                    nuevoGamePlayer.Gameid = Game.id;
                    nuevoGamePlayer.Playerid = IdSession;

                    _repositoryGP.Save(nuevoGamePlayer);

                    return StatusCode(201, nuevoGamePlayer.Id);
                }
            }
            catch(Exception ex)
            {
                return StatusCode(500, "Error del servidor.");
            }
        }
    }
}
  