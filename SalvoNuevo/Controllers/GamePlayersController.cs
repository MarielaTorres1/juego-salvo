﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SalvoNuevo.Models;
using SalvoNuevo.ModelsDTO;
using SalvoNuevo.Repositories;

namespace SalvoNuevo.Controllers
{

    [Route("api/gamePlayers")]
    //[Route("api/[controller]")]
    [ApiController]
    [Authorize("PlayerOnly")]

    public class GamePlayersController : ControllerBase
    {


        private string userEmail;
        private IGamePlayerRepository _repository;

        public GamePlayersController(IGamePlayerRepository repository)
        {
            _repository = repository;
        }

        [HttpGet]
        public IActionResult Get()
        {
            var gameplayers = _repository.GetGamePlayerList();
            return Ok(gameplayers);
        }


        [HttpGet("{id}")]
        public IActionResult Get(long id)
        {
            userEmail = User.FindFirst("Player") != null ? User.FindFirst("Player").Value : "Guest";
            var gameplayers = _repository.GetGamePlayerDTO(id);
            if (gameplayers.Player.Email == userEmail)
            {
                return Ok(new GameViewDTO(gameplayers));
            }
            else
            {
                return StatusCode(403, "Forbidden");
            }

        }

        ///api/gameplayers/4/ships
        [HttpPost("{id}/ships")]
        public IActionResult Post(long id, [FromBody] List<ShipDTO> shipDTO)
        {
            string EmailSession = User.FindFirst("Player").Value;
            GamePlayer gamePlayer = _repository.FindById(id);

            if (gamePlayer == null)
            {
                return StatusCode(403, "No existe el juego.");

            }
            else if (gamePlayer.Player.Email != EmailSession)
            {
                return StatusCode(403, "El usuario no se encuentra en el juego.");
            }

            else if (gamePlayer.Ships.Count != 0)
            {
                return StatusCode(403, "Ya se han posicionado los barcos");
            }

            else
            {
                List<Ship> ships = new List<Ship>();
                foreach (ShipDTO sh in shipDTO)
                {
                    List<ShipLocations> listaloc = new List<ShipLocations>();
                    foreach (ShipLocationsDTO location in sh.locations)
                    {
                        listaloc.Add(new ShipLocations
                        {
                            Location = location.location
                        });
                    }
                    Ship x = new Ship
                    {
                        Type = sh.type,
                        Locations = listaloc
                    };
                    ships.Add(x);
                }

                gamePlayer.Ships = ships;
                _repository.Save(gamePlayer);

                return StatusCode(201, gamePlayer.Id);
            }

        }
        ///api/gameplayers/4/salvos
        [HttpPost("{id}/salvos")]
        //public IActionResult Pos(long id, [FromBody] SalvoDTO salvoDTO)
        public IActionResult Pos(long id, [FromBody] Salvo salvoDTO)
        {
                
                string EmailSession = User.FindFirst("Player").Value;
                GamePlayer gamePlayer = _repository.GetGamePlayerDTO(id);
                GamePlayer gameplayerOpponent = gamePlayer.GetOpponent();
                List<Salvo> salvos;
                List<Salvo> salvosOpp;
            try


            {         
                if (gamePlayer == null)
                {
                    return StatusCode(403, "No existe el juego");
                }

                if (gamePlayer.Player.Email != EmailSession)
                {
                    return StatusCode(403, "El usuario no se encuentra en el juego");
                }
                if (gameplayerOpponent == null)
                {
                    return StatusCode(403, "No existe oponente");
                }
                if (gamePlayer.Salvos == null)
                {
                   //return StatusCode(403, "Error servidor para extraer tus salvos");
                    salvos = new List<Salvo>();
                }

                salvos = gamePlayer.Salvos.ToList();

                if (gameplayerOpponent.Salvos == null)
                {
                    salvosOpp = new List<Salvo>();

                    // return StatusCode(403, "Error servidor");
                }
                else
                {
                    salvosOpp = gameplayerOpponent.Salvos.ToList();
                }


                if ((gamePlayer.CreationDate < gameplayerOpponent.CreationDate
                    && salvos.Count == salvosOpp.Count)
                    || (gamePlayer.CreationDate  > gameplayerOpponent.CreationDate
                    && salvos.Count < salvosOpp.Count))
                {


                    List<SalvoLocations> listaloc = new List<SalvoLocations>();
                    foreach (var location in salvoDTO.Locations)
                    {
                        listaloc.Add(new SalvoLocations
                        {
                            Location = location.Location
                        });
                    }
                    Salvo x = new Salvo
                    {
                        Turn = salvos.Count + 1,
                        Locations = listaloc
                    };
                    salvos.Add(x);

                    gamePlayer.Salvos = salvos;
                    _repository.Save(gamePlayer);
                    return StatusCode(201, gamePlayer.Id);
                }
                else
                {
                    return StatusCode(403, "No se puede adelantar el turno");
                }
            }

            catch (Exception ex)
            {
                return StatusCode(500, "Error de servidor");
            }
        }
    }
}
        

