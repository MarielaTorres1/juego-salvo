﻿using SalvoNuevo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SalvoNuevo.ModelsDTO
{
    public class HitsLocationsDTO
    {
        public string type { get; set; }
        public List<string> hits { get; set; }

        public HitsLocationsDTO(Salvo salvoOP, Ship misShips)
        {
            hits = new List<string>();
            type = misShips.Type;
            var list = getHitsLocations(salvoOP, misShips);
            foreach (var item in list)
            {
                hits.Add(item);
            }
        }
        static public List<string> getHitsLocations(Salvo salvoOP, Ship ship)
        {
            List<string> hits = new List<string>();
            foreach (var slocation in ship.Locations)
            {
                foreach (var salvolocation in salvoOP.Locations)
                    if (slocation.Location == salvolocation.Location)
                    {
                        hits.Add(salvolocation.Location);
                    }
            }
            return hits;
        }

        }
}
