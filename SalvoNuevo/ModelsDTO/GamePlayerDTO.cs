﻿using SalvoNuevo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SalvoNuevo.ModelsDTO
{
    public class GamePlayerView
    {

        public long id { get; set; }
        public PlayerDTO Player { get; set; }
        public double? Point { get; set; }

        public GamePlayerView(GamePlayer gameplayer)
        {
            id = gameplayer.Id;
            Player = new PlayerDTO(gameplayer.Player);
        }
        public GamePlayerView()
        {
        }

}
    public class GamePlayerDTO
    {
        public long id { get; set; }
        public PlayerDTO Player { get; set; }
        public double? Point { get; set; }
        public GamePlayerDTO(GamePlayer gameplayer)
        {

            id = gameplayer.Id;
            Player = new PlayerDTO(gameplayer.Player);
            try
            {
                Point = gameplayer.Player.GetScore(gameplayer.Game).Point;
            }
            catch (Exception)
            {
                Point = 0;
            }
         
        }
        public GamePlayerDTO()
        {
        }

        public List<string> getHits(Salvo salvos)
        {
            List<string> hits = new List<string>();
            foreach (var ship in salvos.GamePlayer.Ships)
            {
                foreach (var slocation in ship.Locations)
                {
                    
                        foreach (var salvolocation in salvos.Locations)
                            if (slocation.Location == salvolocation.Location)
                            {
                                hits.Add(salvolocation.Location);
                            }
                }
            }
            return hits;
        }
        //public getSunks()
        //{

        //}
    }
 }
