﻿using SalvoNuevo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SalvoNuevo.ModelsDTO
{
    public class HitsDTO
    {
        public int turn { get; set; }
        public List<HitsLocationsDTO> hits { get; set; }
        public HitsDTO(Salvo salvoOpp, ICollection<Ship> ships)
        {
            hits = new List<HitsLocationsDTO>();
            turn = salvoOpp.Turn;
            foreach (var ship in ships)
            {
                hits.Add(new HitsLocationsDTO(salvoOpp, ship));
            }
        }
    }
}
