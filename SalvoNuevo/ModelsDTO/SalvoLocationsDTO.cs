﻿using SalvoNuevo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SalvoNuevo.ModelsDTO
{
    public class SalvoLocationsDTO
    {
        public long Id { get; set; }
        public string Location { get; set; }

        public SalvoLocationsDTO (SalvoLocations Locations)
        {
            Id = Locations.Id;
            Location = Locations.Location;
        }
        public SalvoLocationsDTO()
        {

        }
    }
}
