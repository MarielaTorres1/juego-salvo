﻿using SalvoNuevo.Models;
using SalvoNuevo.ModelsDTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static SalvoNuevo.ModelsDTO.SalvoDTO;

namespace SalvoNuevo.Models
{
    public class GameDTO
    {
        public long id { get; set; }
        public DateTime? created { get; set; }
        public List<GamePlayerDTO> gamePlayers { get; set; }
        public GameDTO(Game game)
        {
            id = game.id;
            created = game.CreationDate;
            gamePlayers = new List<GamePlayerDTO>();

            foreach (var gamePlayer in game.GamePlayer)
            {
                gamePlayers.Add(new GamePlayerDTO(gamePlayer));
            }
        }
        public GameDTO()
        { }
    }
}

        