﻿using SalvoNuevo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SalvoNuevo.ModelsDTO
{
    public class ShipDTO
    {
        public long id { get; set; }
        public string type { get; set; }
        public List<ShipLocationsDTO> locations { get; set; }

        public ShipDTO()
        { 
        }
        public ShipDTO(Ship ship)
        {
            id = ship.Id;
            type = ship.Type;
            locations = new List<ShipLocationsDTO>();
            foreach (var loc in ship.Locations)
            {
                locations.Add(new ShipLocationsDTO(loc));
            }
        }
    }

}