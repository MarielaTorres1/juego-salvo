﻿using SalvoNuevo.Models;
using SalvoNuevo.ModelsDTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static SalvoNuevo.ModelsDTO.SalvoDTO;

namespace SalvoNuevo.Models
{
    public class GameViewDTO
    {
        public long id { get; set; }
        public DateTime? created { get; set; }
        public List<GamePlayerDTO> gamePlayers { get; set; }
        public List<ShipDTO> ships { get; set; }
        public List<SalvoViewConLocation> salvos { get; set; }
        public List<HitsDTO> Hits { get; set; }
        public List<HitsDTO> HitsOpponent { get; set; }
        public List<string> Sunks { get; set; }
        public List<string> SunksOpponent { get; set; }

        public GameViewDTO(GamePlayer gamePlayer)
        {
            created = gamePlayer.Game.CreationDate;
            id = gamePlayer.Id;
            gamePlayers = new List<GamePlayerDTO>();
            salvos = new List<SalvoViewConLocation>();
            ships = new List<ShipDTO>();
            Hits = new List<HitsDTO>();
            HitsOpponent = new List<HitsDTO>();
            Sunks = new List<string>();
            SunksOpponent = new List<string>();


            foreach (GamePlayer gp in gamePlayer.Game.GamePlayer)
            {
                gamePlayers.Add(new GamePlayerDTO(gp));

                foreach (Salvo salv in gamePlayer.Salvos)
                {
                    salvos.Add(new SalvoViewConLocation(salv));
                    if (gamePlayer.Game.GamePlayer.Count == 2)
                    {
                        if (gp.Id == gamePlayer.Id)
                        {
                            Hits.Add(new HitsDTO(salv, gamePlayer.GetOpponent().Ships));

                            Sunks = getSunks(gamePlayer.GetOpponent().Salvos.ToList(), gamePlayer.Ships.ToList());

                            //foreach (Ship shipMio in gamePlayer.Ships)
                            //{
                              
                            //}

                        }
                        else
                        {
                                HitsOpponent.Add(new HitsDTO(salv, gamePlayer.Ships));
                                SunksOpponent = getSunks(gamePlayer.Salvos.ToList(), gamePlayer.GetOpponent().Ships.ToList());

                            //foreach (Ship shipMio in gamePlayer.GetOpponent().Ships)
                            //{
                            //}
                        }
                    }
                }
            }

            foreach (Ship sp in gamePlayer.Ships)
            {
                ships.Add(new ShipDTO(sp));

            }

        }
        public List<string> getSunks(List<Salvo> salvoOP, List<Ship> ships)
        {
            int cantHits = 0;
            List<string> sunks = new List<string>();
            foreach (var sh in ships)
            {
                int lonShip = sh.Locations.Count();
                foreach (var locSh in sh.Locations)
                {
                    foreach (var salv in salvoOP)
                    {
                        foreach (var locSalv in salv.Locations)
                        {
                            if (locSalv.Location == locSh.Location)
                            {
                                cantHits++;
                                if(lonShip < cantHits)
                                {
                                    sunks.Add(sh.Type);
                                }
                            }
                        }
                    }
                }
            }

            return sunks;
        }

        public GameViewDTO()
        {

        }
    }
}
