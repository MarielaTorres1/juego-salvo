﻿using SalvoNuevo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SalvoNuevo.ModelsDTO
{
    public class ShipLocationsDTO
    {
        public long id { get; set; }
        public string location { get; set; }
        public ShipLocationsDTO()
        {

        }

        public ShipLocationsDTO(ShipLocations shipLocations)
        {
            id = shipLocations.Id;
            location = shipLocations.Location;
        }
    }
}

