﻿using SalvoNuevo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SalvoNuevo.ModelsDTO
{
    public class GuestPlayerDTO
    {
        public string Email { get; set; }
        public List<GameDTO> Games { get; set; }

        public GuestPlayerDTO(List<GameDTO> game, String email)
        {
            Email = email;
            Games = game;
        }
        public GuestPlayerDTO()
        { }
    }
}
