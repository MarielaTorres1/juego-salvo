﻿using SalvoNuevo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SalvoNuevo.ModelsDTO
{
    public class SalvoDTO
    {
        public long Id { get; set; }
        public int Turn { get; set; }
        public PlayerDTO Player { get; set; }
        public List<SalvoLocationsDTO> Locations { get; set; }
        public SalvoDTO()
        { }
        public SalvoDTO(Salvo salvo)
        {
            Id = salvo.Id;
            Turn = salvo.Turn;
            Player = new PlayerDTO(salvo.GamePlayer.Player);
            Locations = new List<SalvoLocationsDTO>();
            foreach (var location in salvo.Locations)
            {
                Locations.Add(new SalvoLocationsDTO(location));
            }
        }
    }
    public class SalvoViewConLocation
    {
        public long Id { get; set; }
        public int Turn { get; set; }
        public PlayerDTO Player { get; set; }
        public List<SalvoLocationsDTO> Locations { get; set; }
        public SalvoViewConLocation(Salvo salvo)
        {
            Id = salvo.Id;
            Turn = salvo.Turn;
            Player = new PlayerDTO(salvo.GamePlayer.Player);
            Locations = new List<SalvoLocationsDTO>();
            foreach (var loc in salvo.Locations)
            {
                Locations.Add(new SalvoLocationsDTO(loc));
            }
        }
        public SalvoViewConLocation()
        {
        }
    }

}
