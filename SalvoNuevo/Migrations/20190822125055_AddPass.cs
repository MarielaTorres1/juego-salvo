﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SalvoNuevo.Migrations
{
    public partial class AddPass : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ShipLocations_Ship_shipId",
                table: "ShipLocations");

            migrationBuilder.RenameColumn(
                name: "shipId",
                table: "ShipLocations",
                newName: "ShipId");

            migrationBuilder.RenameIndex(
                name: "IX_ShipLocations_shipId",
                table: "ShipLocations",
                newName: "IX_ShipLocations_ShipId");

            migrationBuilder.AddForeignKey(
                name: "FK_ShipLocations_Ship_ShipId",
                table: "ShipLocations",
                column: "ShipId",
                principalTable: "Ship",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ShipLocations_Ship_ShipId",
                table: "ShipLocations");

            migrationBuilder.RenameColumn(
                name: "ShipId",
                table: "ShipLocations",
                newName: "shipId");

            migrationBuilder.RenameIndex(
                name: "IX_ShipLocations_ShipId",
                table: "ShipLocations",
                newName: "IX_ShipLocations_shipId");

            migrationBuilder.AddForeignKey(
                name: "FK_ShipLocations_Ship_shipId",
                table: "ShipLocations",
                column: "shipId",
                principalTable: "Ship",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
