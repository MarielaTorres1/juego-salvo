﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SalvoNuevo.Migrations
{
    public partial class addPasswordToPlayerEntity : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Password",
                table: "Player",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Password",
                table: "Player");
        }
    }
}
