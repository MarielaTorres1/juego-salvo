﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SalvoNuevo.Migrations
{
    public partial class updateSalvoLocation : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_SalvoLocations_Salvos_IdSalvoId",
                table: "SalvoLocations");

            migrationBuilder.RenameColumn(
                name: "IdSalvoId",
                table: "SalvoLocations",
                newName: "SalvoId");

            migrationBuilder.RenameIndex(
                name: "IX_SalvoLocations_IdSalvoId",
                table: "SalvoLocations",
                newName: "IX_SalvoLocations_SalvoId");

            migrationBuilder.AddForeignKey(
                name: "FK_SalvoLocations_Salvos_SalvoId",
                table: "SalvoLocations",
                column: "SalvoId",
                principalTable: "Salvos",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_SalvoLocations_Salvos_SalvoId",
                table: "SalvoLocations");

            migrationBuilder.RenameColumn(
                name: "SalvoId",
                table: "SalvoLocations",
                newName: "IdSalvoId");

            migrationBuilder.RenameIndex(
                name: "IX_SalvoLocations_SalvoId",
                table: "SalvoLocations",
                newName: "IX_SalvoLocations_IdSalvoId");

            migrationBuilder.AddForeignKey(
                name: "FK_SalvoLocations_Salvos_IdSalvoId",
                table: "SalvoLocations",
                column: "IdSalvoId",
                principalTable: "Salvos",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
