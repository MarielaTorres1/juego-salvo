﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SalvoNuevo.Migrations
{
    public partial class updateInitializer : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Salvos_GamePlayer_gamePlayerId",
                table: "Salvos");

            migrationBuilder.DropForeignKey(
                name: "FK_Ship_GamePlayer_gamePlayerID",
                table: "Ship");

            migrationBuilder.RenameColumn(
                name: "lenght",
                table: "Ship",
                newName: "Lenght");

            migrationBuilder.RenameColumn(
                name: "gamePlayerID",
                table: "Ship",
                newName: "GamePlayerID");

            migrationBuilder.RenameIndex(
                name: "IX_Ship_gamePlayerID",
                table: "Ship",
                newName: "IX_Ship_GamePlayerID");

            migrationBuilder.RenameColumn(
                name: "gamePlayerId",
                table: "Salvos",
                newName: "GamePlayerId");

            migrationBuilder.RenameIndex(
                name: "IX_Salvos_gamePlayerId",
                table: "Salvos",
                newName: "IX_Salvos_GamePlayerId");

            migrationBuilder.RenameColumn(
                name: "UserName",
                table: "Player",
                newName: "Email");

            migrationBuilder.AddForeignKey(
                name: "FK_Salvos_GamePlayer_GamePlayerId",
                table: "Salvos",
                column: "GamePlayerId",
                principalTable: "GamePlayer",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Ship_GamePlayer_GamePlayerID",
                table: "Ship",
                column: "GamePlayerID",
                principalTable: "GamePlayer",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Salvos_GamePlayer_GamePlayerId",
                table: "Salvos");

            migrationBuilder.DropForeignKey(
                name: "FK_Ship_GamePlayer_GamePlayerID",
                table: "Ship");

            migrationBuilder.RenameColumn(
                name: "Lenght",
                table: "Ship",
                newName: "lenght");

            migrationBuilder.RenameColumn(
                name: "GamePlayerID",
                table: "Ship",
                newName: "gamePlayerID");

            migrationBuilder.RenameIndex(
                name: "IX_Ship_GamePlayerID",
                table: "Ship",
                newName: "IX_Ship_gamePlayerID");

            migrationBuilder.RenameColumn(
                name: "GamePlayerId",
                table: "Salvos",
                newName: "gamePlayerId");

            migrationBuilder.RenameIndex(
                name: "IX_Salvos_GamePlayerId",
                table: "Salvos",
                newName: "IX_Salvos_gamePlayerId");

            migrationBuilder.RenameColumn(
                name: "Email",
                table: "Player",
                newName: "UserName");

            migrationBuilder.AddForeignKey(
                name: "FK_Salvos_GamePlayer_gamePlayerId",
                table: "Salvos",
                column: "gamePlayerId",
                principalTable: "GamePlayer",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Ship_GamePlayer_gamePlayerID",
                table: "Ship",
                column: "gamePlayerID",
                principalTable: "GamePlayer",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
