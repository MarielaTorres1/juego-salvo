﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SalvoNuevo.Migrations
{
    public partial class ASD : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Score_Game_Gameid",
                table: "Score");

            migrationBuilder.RenameColumn(
                name: "Gameid",
                table: "Score",
                newName: "GameId");

            migrationBuilder.RenameIndex(
                name: "IX_Score_Gameid",
                table: "Score",
                newName: "IX_Score_GameId");

            migrationBuilder.AlterColumn<double>(
                name: "Point",
                table: "Score",
                nullable: true,
                oldClrType: typeof(double));

            migrationBuilder.AddForeignKey(
                name: "FK_Score_Game_GameId",
                table: "Score",
                column: "GameId",
                principalTable: "Game",
                principalColumn: "id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Score_Game_GameId",
                table: "Score");

            migrationBuilder.RenameColumn(
                name: "GameId",
                table: "Score",
                newName: "Gameid");

            migrationBuilder.RenameIndex(
                name: "IX_Score_GameId",
                table: "Score",
                newName: "IX_Score_Gameid");

            migrationBuilder.AlterColumn<double>(
                name: "Point",
                table: "Score",
                nullable: false,
                oldClrType: typeof(double),
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Score_Game_Gameid",
                table: "Score",
                column: "Gameid",
                principalTable: "Game",
                principalColumn: "id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
